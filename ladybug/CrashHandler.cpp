#include "stdafx.h"
#include "CrashHandler.h"
#include <assert.h>
#include <atlfile.h>

// Workaround MS's warnings
#pragma warning (push)
#pragma warning (disable: 4577 4530)
#include <string>
#pragma warning (pop)

// Workaround MS's warnings
#pragma warning(push)
#pragma warning(disable : 4091)
#include <Dbghelp.h>
#pragma warning(pop)

#pragma comment ( lib, "dbghelp.lib" )

class CrashLogFile : public CAtlFile
{
	// This is a plan old ansii file, not unicode compliant
public:

	void log(const char *format, ...)
	{
		va_list vargs;
		va_start(vargs, format);

		char buffer[1024] = { 0 };
		int len = vsprintf_s(buffer, sizeof(buffer), format, vargs);
		len += sprintf_s(buffer + len, sizeof(buffer) - len, "\n");

		va_end(vargs);

		Write(buffer, len);
	}
};

namespace CrashHandler 
{
	static LPTOP_LEVEL_EXCEPTION_FILTER originalExceptionFilter = nullptr;
	static TCHAR szAppName[MAX_PATH] = { 0 };
	static TCHAR szLogFile[MAX_PATH] = { 0 };

	void Register(
		LPCTSTR pszAppName /*= _T("CrashHandler")*/,
		LPCTSTR pszLogFile /*= _T("Crash.rpt")*/
	)
	{
		assert(!originalExceptionFilter || !"Please only call Register ONCE");

		lstrcpyn(szAppName, pszAppName, sizeof(szAppName));
		lstrcpyn(szLogFile, pszLogFile, sizeof(szLogFile));

		originalExceptionFilter = SetUnhandledExceptionFilter(UnhandledExceptionFilter);
	}	

	void Unregister()
	{
		SetUnhandledExceptionFilter(originalExceptionFilter);
		originalExceptionFilter = nullptr;
		ZeroMemory(szAppName, sizeof(szAppName));
		ZeroMemory(szLogFile, sizeof(szLogFile));
	}

	LONG WINAPI UnhandledExceptionFilter(_In_ struct _EXCEPTION_POINTERS *ExceptionInfo)
	{
		// Human readable logging of this crash
		CrashLogFile logFile;
		logFile.Create(szLogFile, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr, nullptr);
		logFile.log("UnhandledExceptionFilter");

		// Write out the minidump
		HANDLE hFile = CreateFile(_T("MiniDump.dmp"), GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if ((hFile != NULL) && (hFile != INVALID_HANDLE_VALUE))
		{
			// Create the minidump 
			MINIDUMP_EXCEPTION_INFORMATION mdei = { 0 };

			mdei.ThreadId          = GetCurrentThreadId();
			mdei.ExceptionPointers = ExceptionInfo;
			mdei.ClientPointers    = FALSE;

			MINIDUMP_TYPE mdt = MiniDumpNormal;
			BOOL rv = MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hFile, mdt, (ExceptionInfo != 0) ? &mdei : 0, 0, 0);

			if (!rv) 
			{
				logFile.log("MiniDumpWriteDump failed. Error: %u", GetLastError());
			}
			else 
			{
				logFile.log("Minidump created.");
			}

			// Close the file 
			CloseHandle(hFile);
		}
		else
		{
			logFile.log("CreateFile failed. Error: %u", GetLastError());
		}
				
		return EXCEPTION_CONTINUE_SEARCH;
	}
}
