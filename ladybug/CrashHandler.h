#pragma once

namespace CrashHandler {
	void Register(LPCTSTR pszAppName = _T("CrashHandler"),
	              LPCTSTR pszLogFile = _T("Crash.rpt"));
	void Unregister();
	LONG WINAPI UnhandledExceptionFilter(_In_ struct _EXCEPTION_POINTERS *ExceptionInfo);
}